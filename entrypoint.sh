#!/bin/bash

# Check if the secret file exists and is not empty
if [ -s /run/secrets/spaces_key ]; then
    export SPACES_KEY=$(cat /run/secrets/spaces_key)
fi

if [ -s /run/secrets/spaces_secret ]; then
    export SPACES_SECRET=$(cat /run/secrets/spaces_secret)
fi

python /home/jovyan/work/api/peper.py
