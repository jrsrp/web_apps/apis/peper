# Dockerfile
FROM continuumio/miniconda3
RUN conda update --yes -c conda-forge  --all  &&  conda install --yes -c conda-forge curl geojson boto3 rasterio[s3] numpy shapely flask flask-cors flask-compress grequests  && conda update --yes -c conda-forge  --all  && conda clean --all --yes

COPY entrypoint.sh /entrypoint.sh

ENTRYPOINT ["/entrypoint.sh"]
