
import datetime
import geojson
import json
import logging
import os

import numpy as np
import rasterio
import rasterio.features
import shapely.geometry
from affine import Affine
from flask import Flask, jsonify, request
from flask_compress import Compress
from flask_cors import CORS
from gevent import monkey
from rasterio.session import AWSSession

monkey.patch_all()

# Set Up Logging
FORMAT = "%(asctime)s %(name)s %(levelname)s %(message)s"
logging.basicConfig(format=FORMAT, level=logging.INFO)
logger = logging.getLogger(__name__)

def is_valid_json(data):
  """
  json has to be a geojson features 
  and you have to have 
  """
  message = None
  # needs to have certain keys
  try:
    for item in data:
      _NONLINEAR_A = item['NONLINEAR_A']
      _NONLINEAR_B = item['NONLINEAR_B']
      geoJsonPoly = item["geoJsonPoly"]
      for feature in geoJsonPoly["features"]:
        geojson_poly_obj = geojson.loads(json.dumps(feature))
        if not geojson_poly_obj.is_valid:
          message = "invalid geojson"
          return False, message

  except KeyError as e:
    return False, f"missing key {str(e)}"

  return True, message
  

def maskedMeanJSON(dataArray):
    if dataArray.count() > 0:
        return dataArray.mean()
    return None


def logCoverFactor(cover):
    cover = np.clip(cover, 0.1, 99.9)
    robModel = -0.000545 * cover**1.9 - 0.802962
    return robModel


def logCover(coverFactor):
    coverFactor = np.clip(coverFactor, logCoverFactor(99.9), logCoverFactor(0.1))
    robModel = (-(coverFactor + 0.802962) / 0.000545) ** (1.0 / 1.9)
    return robModel


def changeRasterMean(coverArray, cfA, cfB):
    newCoverArray = coverArray.copy()
    for i in range(len(newCoverArray)):
        if newCoverArray[i].count() > 0:
            # Get the current mean value of cover after the transform
            currentCoverFactorMean = logCoverFactor(newCoverArray[i]).mean()
            wantedCoverFactorMean = np.log(float(cfA)) + (
                currentCoverFactorMean * float(cfB)
            )
            wantedCoverMean = logCover(wantedCoverFactorMean)
            # Loop until the mean is acceptably close
            while abs(newCoverArray[i].mean() - wantedCoverMean) > 1:
                # Change every value so that the mean will work out OK
                newCoverArray[i] = (
                    newCoverArray[i] + wantedCoverMean - newCoverArray[i].mean()
                )
                # Fix up the raster where the values are incorrect
                newCoverArray[i, newCoverArray[i] > 100] = 100
                newCoverArray[i, newCoverArray[i] < 0] = 0
    return newCoverArray


peperApp = Flask(__name__)
CORS(peperApp)
Compress(peperApp)


# handle grazold polygon
def coverdata_grazproj(polygon, cfA, cfB, dataset, version=2):
    """
    extract data from dataset using polygon
    if version is 2, we return a reduced set of properties
    """
    logger.info("********** Starting PEPER API **********")
    logger.info(
        datetime.datetime.now().strftime("%Y-%m-%dT%H:%M:%S%z ")
        + str(cfA)
        + " "
        + str(cfB)
        + "\n"
        + json.dumps(polygon)
        + "\n"
    )

    geom = shapely.geometry.shape(polygon["geometry"])
    #outpolygon = polygon.copy()
    # we actually want to return it in place
    outpolygon = polygon
    del outpolygon["geometry"]
    # Get pixel coordinates of the geometry's bounding box
    ul = dataset.index(*geom.bounds[0:2])
    lr = dataset.index(*geom.bounds[2:4])
    # Clip bounds
    ul = (np.clip(ul[0], 0, dataset.shape[0]), np.clip(ul[1], 0, dataset.shape[1]))
    lr = (np.clip(lr[0], 0, dataset.shape[0]), np.clip(lr[1], 0, dataset.shape[1]))
    # Guess the request size
    numPixels = (ul[0] + 1 - lr[0]) * (lr[1] + 1 - ul[1])
    # Only run if the size is more than one pixel or less than 100 x 100km
    if numPixels <= 0 or numPixels >= 11111111:
        return None
    window = ((lr[0], ul[0] + 1), (ul[1], lr[1] + 1))
    # Create an affine transform for the subset data
    t = dataset.transform
    shifted_affine = Affine(t.a, t.b, t.c + ul[1] * t.a, t.d, t.e, t.f + lr[0] * t.e)
    # rasterize the geometry
    mask = np.ma.masked_equal(
        rasterio.features.rasterize(
            [(geom, 1)],
            out_shape=dataset.read(1, window=window).shape,
            transform=shifted_affine,
            fill=0,
            all_touched=True,
            dtype=float,
        ),
        0,
    )
    logger.debug("mask created")
    # Extract and mask the band data
    dataBand = (
        np.ma.masked_equal(
            dataset.read(range(1, dataset.count + 1), window=window).astype("float"),
            dataset.nodata,
        )
        * mask
    )
    coverData = dataBand[:-1]
    rksData = dataBand[-1] / 10.0  # It is scaled
    # print(coverData.shape,coverData.mean(),rksData.mean())

    # Compute Cover Factor
    coverFactor = logCoverFactor(coverData)
    # Compute Erosion
    erosionRate = np.exp(coverFactor + rksData)

    # Update for management change
    newCoverData = changeRasterMean(coverData, cfA, cfB)
    newCoverFactor = logCoverFactor(newCoverData)
    newErosionRate = np.exp(newCoverFactor + rksData)
    # Update JSON
    outpolygon["properties"]["NONLINEAR_A"] = cfA
    outpolygon["properties"]["NONLINEAR_B"] = cfB
    outpolygon["properties"]["currentErosionRateMean"] = maskedMeanJSON(erosionRate)
    outpolygon["properties"]["targetErosionRateMean"] = maskedMeanJSON(newErosionRate)
    outpolygon["properties"]["meanCover"] = maskedMeanJSON(coverData)
    outpolygon["properties"]["targetCover"] = maskedMeanJSON(newCoverData)
    if version == 1:
        outpolygon["properties"]["meanCover2015"] = maskedMeanJSON(coverData[0])
        outpolygon["properties"]["meanCover2016"] = maskedMeanJSON(coverData[1])
        outpolygon["properties"]["meanCover2017"] = maskedMeanJSON(coverData[2])
        outpolygon["properties"]["meanCover2018"] = maskedMeanJSON(coverData[3])
        outpolygon["properties"]["meanCover2019"] = maskedMeanJSON(coverData[4])
        outpolygon["properties"]["currentErosionRate2015"] = maskedMeanJSON(
            erosionRate[0]
        )
        outpolygon["properties"]["currentErosionRate2016"] = maskedMeanJSON(
            erosionRate[1]
        )
        outpolygon["properties"]["currentErosionRate2017"] = maskedMeanJSON(
            erosionRate[2]
        )
        outpolygon["properties"]["currentErosionRate2018"] = maskedMeanJSON(
            erosionRate[3]
        )
        outpolygon["properties"]["currentErosionRate2019"] = maskedMeanJSON(
            erosionRate[4]
        )

        outpolygon["properties"]["targetErosionRate2015"] = maskedMeanJSON(
            newErosionRate[0]
        )
        outpolygon["properties"]["targetErosionRate2016"] = maskedMeanJSON(
            newErosionRate[1]
        )
        outpolygon["properties"]["targetErosionRate2017"] = maskedMeanJSON(
            newErosionRate[2]
        )
        outpolygon["properties"]["targetErosionRate2018"] = maskedMeanJSON(
            newErosionRate[3]
        )
        outpolygon["properties"]["targetErosionRate2019"] = maskedMeanJSON(
            newErosionRate[4]
        )
    return outpolygon


def process_data(rawPost, rasterName, base_uri="s3://vegmachine/", version=2):
    """
    Same approach for each endpoint, just
    different rastername
    We return a list, an element for each item in the rawPost
    a single element is a dict with a single key 'feature'
    which is a list of features.
    """
    path = f"{base_uri}{rasterName}"
    returnList = []
    # setup access to image data
    rio_session = AWSSession(
        aws_access_key_id=os.getenv("SPACES_KEY"),
        aws_secret_access_key=os.getenv("SPACES_SECRET"),
        endpoint_url="sfo3.digitaloceanspaces.com",
        requester_pays=True,
        region_name="us-east-1",
    )

    with rasterio.Env(session=rio_session):
        with rasterio.open(path) as dataset:
            for item in rawPost:
                out_features = []
                geoJsonPoly = item["geoJsonPoly"]
                cfA = item["NONLINEAR_A"]
                cfB = item["NONLINEAR_B"]
                for feature in geoJsonPoly["features"]:
                    extract_result = coverdata_grazproj(feature, cfA, cfB, dataset, version=version)
                    out_features.append(extract_result)
                geoJsonPoly['features'] = out_features
                returnList.append(geoJsonPoly)
    return returnList


@peperApp.route("/grazprojold", methods=["POST"])
def peperStats():
    rasterName = "lztmre_qld_e202209201809_peper.tif"
    # Get the JSON we need
    try:
        raw_post = json.loads(request.get_data())
    except Exception as e:
      message = "POST request must include a JSON Collection"
      return jsonify({"error": "Invalid request", "message": str(e)}), 400

    valid, message = is_valid_json(raw_post)
    if not valid:
        return jsonify({"error": "Invalid GeoJSON", "message": message}), 400

    result = process_data(raw_post, rasterName, version=1)
    return jsonify(result)


@peperApp.route("/grazproj", methods=["POST"])
def peperStats2():
    """
    This is the same as peperStats but uses a different file
    and has fewer properties in the output
    """
    rasterName = "QLD_USLE_RKSL_STCK_100M_X_COG.tif"
    # Get the JSON we need
    try:
        raw_post = json.loads(request.get_data())
    except Exception as e:
      message = "POST request must include a JSON Collection"
      return jsonify({"error": "Invalid request", "message": str(e)}), 400

    valid, message = is_valid_json(raw_post)
    if not valid:
        return jsonify({"error": "Invalid GeoJSON", "message": message}), 400

    result = process_data(raw_post, rasterName, version=2)
    return jsonify(result)

@peperApp.route("/grazproj_v2", methods=["POST"])
def peperStats3():
    """
    I can't see any difference here except for the input file.
    """
    rasterName = "QLD_USLE_RKS_STCK_100M_X_COG.tif"
    # Get the JSON we need
    try:
        raw_post = json.loads(request.get_data())
    except Exception as e:
      message = "POST request must include a JSON Collection"
      return jsonify({"error": "Invalid request", "message": str(e)}), 400

    valid, message = is_valid_json(raw_post)
    if not valid:
        return jsonify({"error": "Invalid GeoJSON", "message": message}), 400

    result = process_data(raw_post, rasterName, version=2)
    return jsonify(result)


@peperApp.route("/grazproj_v3", methods=["POST"])
def peperStats4():
    """
    This also looks the same, just different input dataset
    """
    # Get the JSON we need
    rasterName = "QLD_USLE_RKS_STCK_100M_X_GBRF_COG.tif"
    # Get the JSON we need
    try:
        raw_post = json.loads(request.get_data())
    except Exception as e:
      message = "POST request must include a JSON Collection"
      return jsonify({"error": "Invalid request", "message": str(e)}), 400

    valid, message = is_valid_json(raw_post)
    if not valid:
        return jsonify({"error": "Invalid GeoJSON", "message": message}), 400

    result = process_data(raw_post, rasterName, version=2)
    return jsonify(result)


@peperApp.route("/isup", methods=["GET"])
def helloworld():
    return "Yes"


if __name__ == "__main__":
    peperApp.run(
        host="0.0.0.0",
        port=int("5001"),
        debug=False,
        use_debugger=False,
        use_reloader=False,
    )
