**PEPER**

**Introduction**

This project implements a Flask-based API named PEPER. It processes geospatial data and calculates erosion rates based on user-defined management changes.

**Functionality**

Accepts GeoJSON features representing areas of interest.
Reads raster data from specified files.
Calculates cover factors and erosion rates for the requested regions.
Updates cover factors based on user-provided coefficients (NONLINEAR_A and NONLINEAR_B).
Returns the updated erosion rates and other relevant information (e.g., mean cover) as JSON for each feature.

**Endpoints**

There are currently four endpoints, each representing a developmental iteration. The current version is /grazproj_v2.

- /grazprojold: Processes data using the lztmre_qld_e202209201809_peper.tif raster file and returns detailed properties for each feature.

- /grazproj: Processes data using the QLD_USLE_RKSL_STACK_100M_X_COG.tif raster file and returns basic erosion rate properties.

- /grazproj_v2: Processes data using the QLD_USLE_RKS_STCK_100M_X_COG.tif raster file and returns basic erosion rate properties.
- /grazproj_v3: Processes data using the QLD_USLE_RKS_STCK_100M_X_GBRF_COG.tif raster file and returns basic erosion rate properties.

- /isup: Basic health check endpoint that responds with "Yes" if the API is running.

The main difference between the versions is the inputs. /grazproj and /grazproj_v2 use different input rasters.
`/grazproj` uses a 12 band raster with 11 years of spring ground cover data (2004-2016) for the first 11 
bands and a RKSL band for the 12th band. The more recent `/grazproj_v2`` uses 11 years of spring ground cover 
data () and the RKS band for the 12th band. All data is publically available from the QLD government.


**Dependencies**

Python 3.x (check specific version requirement if needed)
- gevent
- shapely
- rasterio
- affine
- numpy
- json
- flask
- flask-cors
- flask-compress
- logging


**Installation**

To read the COGS which are held on an s3 type cloud storage, you will require
a key and token. Contact the maintainers for further details on access.


1. Clone this repository:

`git clone https://gitlab.com/jrsrp/web_apps/apis/peper.git`



2. Install dependencies:

`pip install -r requirements.txt`



**Usage**

1. Run the API server:

`python main.py`



2. Send POST requests to the appropriate endpoint with GeoJSON data and management change coefficients (NONLINEAR_A and NONLINEAR_B) in the request body.

**Example Usage (using /grazprojold endpoint):**

```
POST http://localhost:5001/grazprojold

{
  "features": [
    {
      "geometry": {
        "type": "Polygon",
        "coordinates": [...]  // GeoJSON polygon coordinates
      },
      "properties": {
        "NONLINEAR_A": 0.5,
        "NONLINEAR_B": 1.2
      }
    },
    // ... other features
  ]
}
```


The response will be a JSON array containing the updated GeoJSON features with additional properties like currentErosionRateMean, targetErosionRateMean, and meanCover.

**Logging**

The API uses Python's logging module to record information about requests, errors, and other events. Logs are written to /home/jovyan/vmapi.log by default.

**Docker**

A sample Docker command is provided in the code for reference. It exposes the API on port 5001 and sets environment variables for logging and virtual hosting.

**Additional Notes**

The code utilizes gunicorn for asynchronous processing (requires gevent).
Error handling is included to catch exceptions and return appropriate messages to the client.


Maintainers:

* Robert Denham (robert.denham@des.qld.go.au)
* Rebecca Trevithick (rebecca.trevithick@des.qld.gov.au)



